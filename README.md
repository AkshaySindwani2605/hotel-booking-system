A Hotel Booking Broker is an online system that allows users to search for, compare rates, check availability and make bookings at the 
hotels connected to the service in various cities throughout Australia. The various hotels with the same name, but in different cities, are unrelated. 
So, for example, the Hilton Hotel in Melbourne is considered totally unrelated to the Hilton Hotel in Perth – and so on.
A generic breakdown of the features looks like :-

• What cities are currently serviced by the system? The response is a list such as: Melbourne, Sydney and Perth.
• For a given city, what hotels are currently serviced by the system? The is a list such as: Hilton, Mariot, Travel Lodge, Intercontinental,
  Holiday Inn and Hotel Vistara.
• For a particular hotel, what is the room rate? (A hotel has one room rate, but the different hotels may have different rates).
• For a particular hotel, does it have a vacancy between given check-in and check-out dates? (We assume all vacancies and bookings should be for July, 2018).
• Book a room at a hotel. 
Completing the booking request includes the user submitting the following information: 
check-in date, check-out date 
guest’s name
contact (phone or e-mail address) and 
credit card number

Please have a look at the Downloads section of the repo to read the Documentation of the entrire project.