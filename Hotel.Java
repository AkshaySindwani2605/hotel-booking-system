//import java.util.*;
import java.text.*;
import java.sql.*;

public class Hotel
{
    Database connection = new Database();
    BasicClient client = new BasicClient();
    BasicServer server = new BasicServer();
    public void pickACity()
    {
        System.out.println("Welcome to Monash Booking System");
        System.out.println("Please select anyone of the following options");
        System.out.println("1)Melbourne");
        System.out.println("2)Sydney");
        System.out.println("3)Perth");
        System.out.println("4)Exit");
        String option;
        java.util.Scanner input = new java.util.Scanner(System.in);
        option = input.next();
        switch(option)
        {
            case "1":   String city = "Melbourne";
                        mel(city);
                        break;
            case "2":   city = "Sydney";
                        syd(city);
                        break;
            case "3":   city = "Perth";
                        per(city);
                        break;
            case "4":   System.out.println("Thanks for using our system. Hope you had a good time. Hope to see you again. Enjoy!!");
                        break;
            default:    System.out.println("Check your input");
                        break;
        }
    }

    public void mel(String city)
    {
        System.out.println("Welcome the hotel booking system for Melbourne");
        System.out.println("Please select anyone of the following options");
        System.out.println("1)Mariot");
        System.out.println("2)Hilton");
        System.out.println("3)Go back to the previous menu");
        System.out.println("4)Exit");
        java.util.Scanner input = new java.util.Scanner(System.in);
        String option = input.next();
        switch(option)
        {
            case "1":   String hotel = "Mariot";
                        pickARoom(city,hotel);
                        break;
            case "2":   hotel = "Hilton";
                        pickARoom(city,hotel);
                        break;
            case "3":   pickACity();
                        break;
            case "4":   System.out.println("Thanks for using this booking system. Hope you found what you were looking for. Enjoy!!");
                        break;
            default:    System.out.println("Check your input");
                        break;
        }
    }

    public void syd(String city)
    {
        System.out.println("Welcome the hotel booking system for Sydney");
        System.out.println("Please select anyone of the following options");
        System.out.println("1)Travel Lodge");
        System.out.println("2)Intercontinental");
        System.out.println("3)Go back to the previous menu");
        System.out.println("4)Exit");
        java.util.Scanner input = new java.util.Scanner(System.in);
        String option = input.next();
        switch(option)
        {
            case "1":   String hotel = "Travel Lodge";
                        pickARoom(city,hotel);    
                        break;
            case "2":   hotel = "Intercontinental";
                        pickARoom(city,hotel);
                        break;
            case "3":   pickACity();
                        break;
            case "4":   System.out.println("Thanks for using this booking system. Hope you found what you were looking for. Enjoy!!");
                        break;
            default:    System.out.println("Check your input");
                        break;
        }
    }

    public void per(String city)
    {
        System.out.println("Welcome the hotel booking system for Perth");
        System.out.println("Please select anyone of the following options");
        System.out.println("1)Holiday Inn");
        System.out.println("2)Hotel Vistara");
        System.out.println("3)Go back to the previous menu");
        System.out.println("4)Exit");
        java.util.Scanner input = new java.util.Scanner(System.in);
        String option = input.next();
        switch(option)
        {
            case "1":   String hotel = "Holiday Inn";
                        pickARoom(city,hotel);
                        break;
            case "2":   hotel = "Hotel Vistara";
                        pickARoom(city,hotel);
                        break;
            case "3":   pickACity();
                        break;
            case "4":   System.out.println("Thanks for using this booking system. Hope you found what you were looking for. Enjoy!!");
                        break;
            default:    System.out.println("Check your input");
                        break;
        }

    }

    public void pickARoom(String city, String hotel)
    {
        System.out.println("There are three kinds of rooms available with us for all the three cities. Please pick one, They are as follows: ");
        System.out.println("1)Basic");
        System.out.println("2)Suite");
        System.out.println("3)Presidential");
        System.out.println("4)Go back to the previous menu");
        System.out.println("5)Exit");
        java.util.Scanner input = new java.util.Scanner(System.in);
        String option = input.next();
        switch (option)
        {
            case "1":   System.out.println("The price of a basic room is $100 per night");
                        String room = "Basic";
                        checkAvailability(city,hotel,room);
                        break;
            case "2":   System.out.println("The price of a suite is $200 per night");
                        room = "Suite";
                        checkAvailability(city,hotel,room);
                        break;
            case "3":   System.out.println("The price of a Presidential suite is $600 per night");
                        room = "Presidential";
                        checkAvailability(city,hotel,room);
                        break;
            case "4":    System.out.println("Type in the code of the city you want to go back to");
                         System.out.println("Mel for Melbourne");
                         System.out.println("Syd for Sydney");
                         System.out.println("Per for Perth");
                         java.util.Scanner scan = new java.util.Scanner(System.in);
                         String cityAgain = scan.next();
                         if(cityAgain.equals("Mel"))
                            {
                                mel(city);
                                break;
                            }
                                if(cityAgain.equals("Syd"))
                                    {
                                        syd(city);
                                        break;
                                    }
                                    if(cityAgain.equals("Per"))
                                        {
                                            per(city);
                                            break;
                                        }
                                            else
                                            System.out.println("Check your input");
                         break;
            case "5":   System.out.println("Thanks for using our Booking system. We hope you got what you were looking for. Hope to see you again.");
                        break;
            default:    System.out.println("Check your input");
                        break;    
        }
    }

    public void checkAvailability(String city, String hotel, String room)
    {
        
        String checkOutDate;
        String checkInDate;
        System.out.println("Its the last step. Select anyone of the following options");
        System.out.println("1)Enter dates to check availability.");
        System.out.println("2)Go back to the previous menu");
        System.out.println("3)Go back to the main menu");
        System.out.println("4)Exit");
        java.util.Scanner input = new java.util.Scanner(System.in);
        String option = input.next();
        switch(option)
        {
            case "1":   System.out.println("Enter your check in dates in the format YYYY-MM-DD");
                        checkInDate = input.next();
                        System.out.println("Enter your check out date in the same format(YYYY-MM-DD)");
                        checkOutDate = input.next();
                        while(checkInDate.equals(checkOutDate))
                        {
                            System.out.println("You have entered the same dates. Please check your dates/input");
                            System.out.println("Enter your check in dates in the format YYYY-MM-DD");
                            checkInDate = input.next();
                            System.out.println("Enter your check out date in the same format(YYYY-MM-DD)");
                            checkOutDate = input.next();
                        }
                        try
                        {
                            Date in = Date.valueOf(checkInDate);
                            Date out = Date.valueOf(checkOutDate);
                            connection.connection(in,out,city,hotel,room);
                        }
                        catch(Exception e)
                        {
                            System.out.println(e);
                        }
                        break;
            case "2":   System.out.println("Type in the code of the city you want to go back to");
                        System.out.println("Mel for Melbourne");
                        System.out.println("Syd for Sydney");
                        System.out.println("Per for Perth");
                        java.util.Scanner scan = new java.util.Scanner(System.in);
                        String cityAgain = scan.next();
                        if(cityAgain.equals("Mel"))
                            {
                                mel(city);
                                break;
                            }
                            if(cityAgain.equals("Syd"))
                                {
                                    syd(city);
                                    break;
                                }
                                if(cityAgain.equals("Per"))
                                    {
                                        per(city);
                                        break;
                                    }
                                        else
                                        System.out.println("Check your input");
                        break;
            case "3":   pickACity();
                        break;
            case "4": System.out.println("Thanks for using this booking system. Hope you found what you were looking for. Enjoy!!");
                      break;
            default:  System.out.println("Check your input");
                      break;
        }
    }

}

                        
 
                        
                        